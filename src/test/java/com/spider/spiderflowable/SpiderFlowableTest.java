package com.spider.spiderflowable;

import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import org.flowable.engine.HistoryService;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.flowable.engine.history.HistoricActivityInstance;
import org.flowable.engine.repository.Deployment;
import org.flowable.engine.repository.ProcessDefinition;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.task.api.Task;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author xiong.bo
 * @version 1.0
 * @date 2022/11/27 10:41 上午
 */

@SpringBootTest
class SpiderFlowableTest {

    @Autowired
    private RepositoryService repositoryService;
    @Autowired
    private RuntimeService runtimeService;
    @Autowired
    private TaskService taskService;
    @Autowired
    private HistoryService historyService;

    /**
     * 模拟-部署流程
     * 说明：
     * 流程定义有版本的概念，bpmn文件有改动，需要部署后才生效
     */
    @Test
    void createDeployment() {
        Deployment deployment = repositoryService.createDeployment()
                .addClasspathResource("processes/holiday-request.bpmn20.xml")
                .deploy();
        System.out.println(deployment.getId());
        System.out.println(JSON.toJSONString(deployment));
    }

    /**
     * 获取流程定义
     */
    @Test
    void getProcessDefinition() {
        ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery()
                .deploymentId("17504")
                .singleResult();
        System.out.println("definitionId:" + processDefinition.getId());
        System.out.println("definition:" + JSONUtil.toJsonStr(processDefinition));
    }

    /**
     * 启动流程
     * 模拟用户发起一个请假流程
     */
    @Test
    void startProcessDefinition() {
        Map<String, Object> variables = new HashMap<>();
        variables.put("employee", "李四");
        variables.put("nrOfHolidays", "4");
        variables.put("description", "外出请假");
        //启动流程实例有多个方法，这里调用流程key的方法来启动
        ProcessInstance holidayProcessInstance = runtimeService.startProcessInstanceByKey("holidayRequest", variables);
        System.out.println("processInstanceId:" + holidayProcessInstance.getProcessInstanceId());
    }

    /**
     * 获取任务
     * 用户发起流程后，相关的人员能够查询该任务
     *
     */
    @Test
    void getTask(){
        List<Task> tasks = taskService.createTaskQuery()
                .active()
                .includeProcessVariables()
                //值为admin是管理员可以查看所有的，测试
                .taskCandidateOrAssigned("admin")
                .list();
        System.out.println("You have " + tasks.size() + " tasks:");
        for (int i = 0; i < tasks.size(); i++) {
            Task task = tasks.get(i);
            System.out.println((i + 1) + ") " + "taskId: " + task.getId() + ", taskName: " + task.getName());
        }
    }

    /**
     * 审批任务
     * 说明：
     * 1，变量approved同流程定义文件里面顺序流定义的变量
     * 2,taskId是上一个获取用户任务的taskId值，也就是要指定哪一个用户任务往下执行
     */
    @Test
    void completeTask(){
        Map<String, Object> variables = new HashMap<>();
        variables.put("approved", true);
        String taskId = "20008";
        taskService.complete(taskId,variables);
    }

    /**
     * 查看历史任务
     * 说明：
     * taskAssignee: 分配人
     * finished：已完成状态的
     *
     */
    @Test
    void historyTask(){
        List<HistoricActivityInstance> hisList = historyService.createHistoricActivityInstanceQuery()
                .taskAssignee("admin")
                .finished()
                .list();

        hisList.stream().forEach(e -> System.out.println(JSONUtil.toJsonStr(e)));

    }


}
