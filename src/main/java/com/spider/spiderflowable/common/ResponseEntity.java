package com.spider.spiderflowable.common;

import lombok.Data;
import org.springframework.http.HttpStatus;

/**
 * @author xiong.bo
 * @version 1.0
 * @date 2022/12/9 8:54 下午
 */
@Data
public class ResponseEntity<T> {

    private int code;

    private String message;

    private T data;

    public static <T> ResponseEntity<T> ok(T data) {
        int code = HttpStatus.OK.value();
        String message = HttpStatus.OK.getReasonPhrase();
        return new ResponseEntity<>(code, message, data);
    }

    public static <T> ResponseEntity<T> error(int code, String message) {
        return new ResponseEntity<>(code, message, null);
    }

    public ResponseEntity(int code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }
}