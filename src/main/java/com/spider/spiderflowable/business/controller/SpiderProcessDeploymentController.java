package com.spider.spiderflowable.business.controller;

import com.spider.spiderflowable.common.ResponseEntity;
import com.spider.spiderflowable.core.service.SpiderProcessDeploymentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.flowable.engine.repository.Deployment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 流程部署接口
 *
 * @author xiong.bo
 * @version 1.0
 * @date 2022/12/9 8:51 下午
 */

@Api(tags = "流程部署接口")
@RequestMapping("/api/deployment")
@RestController
public class SpiderProcessDeploymentController {

    @Autowired
    private SpiderProcessDeploymentService deploymentService;


    @ApiOperation(value = "部署流程-根据流程文件URL（一般用来本地测试）")
    @PostMapping("/release-url")
    public ResponseEntity<Deployment> createDeployment(String processName, String processUrl) {
        return ResponseEntity.ok(deploymentService.createDeployment(processName, processUrl));
    }

    @ApiOperation(value = "部署流程-根据资源相对路径（一般用来本地测试）")
    @PostMapping("/release-resource")
    public ResponseEntity<Deployment> exportUserData(String classpathResource) {
        return ResponseEntity.ok(deploymentService.createDeployment(classpathResource));
    }


}
