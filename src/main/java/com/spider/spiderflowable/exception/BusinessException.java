package com.spider.spiderflowable.exception;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

/**
 * 业务异常类
 *
 * @author xiong.bo
 * @version 1.0
 * @date 2024/5/6 7:35 下午
 */

@Data
@NoArgsConstructor
public class BusinessException extends RuntimeException {

    private Integer code;

    private String message;

    public BusinessException(String message) {
        this.message = message;
        code = HttpStatus.INTERNAL_SERVER_ERROR.value();
    }

    public BusinessException(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    /**
     * @param code    错误码
     * @param message 错误信息
     * @param cause   原始异常对象
     */
    public BusinessException(Integer code, String message, Throwable cause) {
        super(cause);
        this.code = code;
        this.message = message;
    }

}

