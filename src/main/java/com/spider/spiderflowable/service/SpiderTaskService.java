package com.spider.spiderflowable.service;

import java.util.Map;

/**
 * @author xiong.bo
 * @version 1.0
 * @date 2024/5/7 8:10 下午
 */
public interface SpiderTaskService {


    /**
     * 签收
     *
     * @param taskId 任务实例ID
     * @param userId 用户ID
     */
    void claim(String taskId, String userId);

    /**
     * 执行任务
     *
     * @param taskId    任务实例ID
     * @param variables 变量数据
     */
    void complete(String taskId, Map<String, Object> variables);

    /**
     * 委派他人来完成任务
     *
     * @param taskId 任务实例ID
     * @param userId 变量数据
     */
    void delegateTask(String taskId, String userId);

    /**
     * 分配任务给某个用户
     *
     * @param taskId 任务实例ID
     * @param userId 用户ID
     */
    void setAssignee(String taskId, String userId);

    /**
     * 得到用户任务的变量数据
     *
     * @param taskId 任务实例ID
     * @return
     */
    Map<String, Object> getVariables(String taskId);


}
