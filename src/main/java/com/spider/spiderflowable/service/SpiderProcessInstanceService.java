package com.spider.spiderflowable.service;

import org.flowable.engine.runtime.ProcessInstance;

import java.util.Map;

/**
 * @author xiong.bo
 * @version 1.0
 * @date 2024/5/6 7:57 下午
 */
public interface SpiderProcessInstanceService {

    /**
     * 启动新的流程实例
     *
     * @param processDefinitionKey 流程定义key
     * @param businessKey          业务key
     * @param variables            变量数据
     * @param tenantId             租户ID
     * @return
     */
    ProcessInstance startProcessInstanceByKeyAndTenantId(String processDefinitionKey, String businessKey, Map<String, Object> variables, String tenantId);

    /**
     * 删除流程实例
     *
     * @param processInstanceId 流程实例ID
     * @param deleteReason      删除原因
     */
    void deleteProcessInstance(String processInstanceId, String deleteReason);

    /**
     * 挂起流程实例
     *
     * @param processInstanceId 流程实例ID
     */
    void suspendProcessInstanceById(String processInstanceId);

    /**
     * 激活流程实例
     *
     * @param processInstanceId 流程实例ID
     */
    void activateProcessInstanceById(String processInstanceId);

    /**
     * 获取流程实例的变量数据Map
     *
     * @param executionId 执行ID
     * @return
     */
    Map<String, Object> getVariables(String executionId);

    /**
     * 获取流程实例的某个变量数据
     *
     * @param executionId  执行Id
     * @param variableName 变量名称
     * @return
     */
    Object getVariable(String executionId, String variableName);
}
