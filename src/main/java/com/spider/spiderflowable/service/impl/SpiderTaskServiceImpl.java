package com.spider.spiderflowable.service.impl;

import com.spider.spiderflowable.service.SpiderTaskService;
import org.flowable.engine.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author xiong.bo
 * @version 1.0
 * @date 2024/5/7 8:18 下午
 */
@Service
public class SpiderTaskServiceImpl implements SpiderTaskService {

    @Autowired
    private TaskService taskService;

    @Override
    public void claim(String taskId, String userId) {
        taskService.claim(taskId, userId);
    }

    @Override
    public void complete(String taskId, Map<String, Object> variables) {
        taskService.complete(taskId, variables);
    }

    @Override
    public void delegateTask(String taskId, String userId) {
        taskService.delegateTask(taskId, taskId);
    }

    @Override
    public void setAssignee(String taskId, String userId) {
        taskService.setAssignee(taskId, userId);
    }

    @Override
    public Map<String, Object> getVariables(String taskId) {
        return taskService.getVariables(taskId);
    }
}
