package com.spider.spiderflowable.core.delegate;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.JavaDelegate;

/**
 *
 * 审核通过回调任务
 *
 * @author xiong.bo
 * @version 1.0
 * @date 2022/11/27 10:27 上午
 */
@Slf4j
public class ApprovalSuccessDelegate implements JavaDelegate {

    @Override
    public void execute(DelegateExecution delegateExecution) {
        log.info("审批通过了");
        log.info("数据内容："+ JSON.toJSONString(delegateExecution));
    }
}
