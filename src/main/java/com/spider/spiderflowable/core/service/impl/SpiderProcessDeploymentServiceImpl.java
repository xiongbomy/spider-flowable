package com.spider.spiderflowable.core.service.impl;

import com.spider.spiderflowable.core.service.SpiderProcessDeploymentService;
import com.spider.spiderflowable.utils.LoadFileUtil;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.repository.Deployment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.InputStream;

/**
 * @author xiong.bo
 * @version 1.0
 * @date 2022/12/9 8:42 下午
 */

@Service
public class SpiderProcessDeploymentServiceImpl implements SpiderProcessDeploymentService {

    @Autowired
    private RepositoryService repositoryService;

    @Override
    public Deployment createDeployment(String classpathResource) {
        return repositoryService.createDeployment()
                .addClasspathResource(classpathResource)
                .deploy();
    }

    @Override
    public Deployment createDeployment(String processName, String processUrl) {
        InputStream inputStream = LoadFileUtil.downLoadFile(processUrl);
        return repositoryService.createDeployment()
                .addInputStream(processName, inputStream)
                .deploy();
    }
}
