package com.spider.spiderflowable.core.service;

import org.flowable.engine.repository.Deployment;

/**
 * 流程部署类
 *
 * @author xiong.bo
 * @version 1.0
 * @date 2022/12/9 8:41 下午
 */
public interface SpiderProcessDeploymentService {


    /**
     * resources下流程文件的相对路径
     *
     * @param classpathResource
     * @return
     */
    Deployment createDeployment(String classpathResource);


    /**
     * 部署流程
     *
     * @param processName 流程定义名称
     * @param processUrl  流程定义文件URL
     * @return
     */
    Deployment createDeployment(String processName, String processUrl);

}
