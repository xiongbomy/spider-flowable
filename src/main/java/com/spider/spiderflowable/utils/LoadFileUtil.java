package com.spider.spiderflowable.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * @author xiong.bo
 * @version 1.0
 * @date 2022/12/9 9:20 下午
 */

public class LoadFileUtil {

    private LoadFileUtil(){}

    public static InputStream downLoadFile(String url) {
        try {
            URL file1 = new URL(url);
            HttpURLConnection httpConnection = (HttpURLConnection) file1.openConnection();
            return httpConnection.getInputStream();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 将InputStream流转化为File文件对象
     *
     * @param inputStream
     * @return
     * @throws IOException
     */
    public static File copyInputStreamToFile(InputStream inputStream) throws IOException {
        File file = new File("tempReadFile.md");//临时读取的文件，要保证路径存在
        try (FileOutputStream outputStream = new FileOutputStream(file)) {
            int read;
            byte[] bytes = new byte[1024];
            while ((read = inputStream.read(bytes)) != -1) {
                outputStream.write(bytes, 0, read);
            }
        }
        return file;
    }
}
