package com.spider.spiderflowable;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Slf4j
@EnableWebMvc
@SpringBootApplication
public class SpiderFlowableApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpiderFlowableApplication.class, args);
        log.info("SpiderFlowableApplication start success!");
    }

}
